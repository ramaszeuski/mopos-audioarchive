package Mopos::AudioArchive;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
    -Debug
    ConfigLoader
    Static::Simple
    Authentication
    Authorization::Roles

    Session
    Session::Store::FastMmap
    Session::State::Cookie

    Unicode::Encoding
/;

extends 'Catalyst';

our $VERSION = '1.5.0';
$VERSION = eval $VERSION;

# Configure the application.
#
# Note that settings in mopos_audioarchive.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
    name => 'Mopos::AudioArchive',

    encoding => 'utf8',

    disable_component_resolution_regex_fallback => 1,
    enable_catalyst_header                      => 1,

    'Plugin::Static::Simple' => {
        include_path => [
            'root/static',
        ],
    },

    'Model::DB' => {
        connect_info => {
            dsn      => $ENV{DB_DSN} // 'dbi:Pg:dbname=audioarchive;host=sql',
            user     => $ENV{DB_USERNAME} // 'audioarchive',
            password => $ENV{DB_PASSWORD} // 'password',
        }
    },

    authentication => {
      default_realm => 'sources',
      realms        => {
#         users => {
#            credential => {
#               class          => 'Password',
#               password_field => 'password',
#               password_type  => 'clear'
#            },
#            store => {
#               class         => 'DBIx::Class',
#               user_model    => 'DB::User',
#               id_field      => 'login',
#               role_column => 'roles',
#            }
#         },
         sources => {
            credential => {
               class          => 'Password',
               password_field => 'password',
               password_type  => 'clear'
            },
            store => {
               class         => 'DBIx::Class',
               user_model    => 'DB::Source',
               id_field      => 'login',
               role_column   => 'url',
            }
         },
         nopassword => {
            credential => {
               class          => 'NoPassword',
            },
            store => {
               class         => 'DBIx::Class',
               user_model    => 'DB::User',
               role_column   => 'roles',
#               role_relation => 'roles',
#               role_field    => 'role',
            }
         },
      },
   },

    'Controller::HTML::FormFu' => {
        constructor => {
            render_method => 'tt',
            tt_args       => {
                ENCODING => 'utf-8',
            },
        },
        model_stash => {
            schema => 'DB',
        },
    },
);

# Start the application
__PACKAGE__->setup();


=head1 NAME

Mopos::AudioArchive - Catalyst based application

=head1 SYNOPSIS

    script/mopos_audioarchive_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<Mopos::AudioArchive::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
