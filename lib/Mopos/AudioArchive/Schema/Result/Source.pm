package Mopos::AudioArchive::Schema::Result::Source;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('source');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        active
        created
        deleted
        login
        password
        name
        firstname
        lastname
        email
        messenger
        phone
        cellular
        fax
        country
        region
        city
        address
        zip
        profile
        notes
        key
        url
        alert_email
        named_relations
        hidden
        last_record_created
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    records => 'Mopos::AudioArchive::Schema::Result::Record',
    {
        'foreign.source_id' => 'self.id'
    },
);

1;
