package Mopos::AudioArchive::Schema::Result::User;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 2;

__PACKAGE__->table('users');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type   => 'integer',
        is_nullable => 0,
    },
    qw(
        active
        created
        deleted
        login
        password
        name
        firstname
        lastname
        email
        roles
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    'login' => [qw(login)]
);

__PACKAGE__->has_many(
    log_event => 'Mopos::AudioArchive::Schema::Result::Log',
    {
        'foreign.user_id' => 'self.id'
    },
);

sub role_names {
    my $self = shift;
    my @roles = ();
    my $resultset = $self->result_source->schema->resultset('User');


    ROLE:
    foreach my $role ( split /\W/, $self->roles ) {
        push @roles, $resultset->role_name( $role );
    }

    return join ', ', @roles;

}

sub name {
    my $self = shift;
    my $name =
        join ' ',
        grep /\w/,
        (
            $self->firstname // '',
            $self->lastname // '',
        );

    return $name || $self->login;

}

sub is_deletable {
    my $self = shift;
    return 1;
}

1;

__END__
