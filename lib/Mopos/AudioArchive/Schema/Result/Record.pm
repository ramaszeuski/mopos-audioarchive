package Mopos::AudioArchive::Schema::Result::Record;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('record');

__PACKAGE__->add_columns(
    id => {
        sequence          => 'uid_seq',
        data_type         => 'integer',
        is_nullable       => 0,
        auto_nextval      => 1,
        is_auto_increment => 1,
    },
    qw(
        source_id
        key
        created
        processed
        deleted
        duration
        name
        description
    ),
);

__PACKAGE__->set_primary_key('id');

#__PACKAGE__->add_unique_constraint( key => ['key'] );

__PACKAGE__->belongs_to(
    source => 'Mopos::AudioArchive::Schema::Result::Source',
    {
        'foreign.id' => 'self.source_id'
    },
);

1;

__END__
