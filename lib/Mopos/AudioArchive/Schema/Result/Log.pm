package Mopos::AudioArchive::Schema::Result::Log;

use strict;
use warnings;

use base 'DBIx::Class::Core';

use JSON;

our $VERSION = 1;

__PACKAGE__->table('log');

__PACKAGE__->add_columns(
    id => {
        sequence    => 'uid_seq',
        data_type         => 'integer',
        is_nullable       => 0,
    },
    user_id => {
        data_type     => 'integer',
        is_nullable   => 1,
    },
    timestamp => {
        data_type     => 'datetime',
        is_nullable   => 0,
        default_value => \'current_timestamp',
    },
    event => {
        data_type     => 'varchar',
        is_nullable   => 0,
        size          => 32,
    },
    address       => {
        data_type     => 'inet',
        is_nullable   => 1,
    },
    data => {
        data_type     => 'text',
    },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->inflate_column('data', {
    inflate => sub { from_json(shift) },
    deflate => sub { to_json(shift) },
});

1;

__END__

