package Mopos::AudioArchive::Schema::ResultSet::User;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

our $VERSION = 1;

use constant ROLE => {
    root  => 'Operator (ROOT)',
    user  => 'Správce zákazníka',
};

sub roles {

    my $self  = shift;
    my $args = shift;

    my @roles = ();

    ROLE:
    foreach my $role ( sort keys %{ ROLE() } ) {
        next ROLE
            if ( $role eq 'root' )
            && ( ! $args->{allow_root} );

        push @roles, { value => $role, label => ROLE->{$role} }

    }

    return [
        sort { $a->{label} cmp $b->{label} } @roles
    ];
}

sub role_name {
    my $self  = shift;
    my $event = shift;
    return ROLE->{$event};
}

1;

