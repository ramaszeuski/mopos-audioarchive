package Mopos::AudioArchive::Schema::ResultSet::Record;

use strict;
use warnings;
use utf8;

use Digest::SHA;
use XML::Simple;
use Date::Manip;
use MP3::Info;
use YAML;

use base 'DBIx::Class::ResultSet';

Date_Init("DateFormat=non-US");

our $VERSION = 1;

sub create_from_file {
    my $self = shift;

    my $source       = shift;
    my $filename     = shift // die '412 Filename required';
    my $xml_filename = shift // die '412 XML Filename required';

    my ( $created, $duration );
    # xml soubor s metadaty

    if ( $xml_filename && -f $xml_filename ) {

            my $xml = XMLin ( $xml_filename );

            if ( $xml->{datum} && $xml->{cas} ) {
                $created = ParseDate($xml->{datum} . ' ' . $xml->{cas});
            }

            if ( $xml->{delka} ) {
                if ($xml->{delka} =~ /(\d\d):(\d\d):(\d\d)/ ) {
                    $duration = $1 * 3600 + $2 * 60 + $3;
                }
                elsif ($xml->{delka} =~ /(\d\d):(\d\d)/ ) {
                    $duration = $1 * 60 + $2;
                }
                elsif ($xml->{delka} =~ /(\d\d)/ ) {
                    $duration = $1;
                }
            }
    }

    if ( ! $duration ) {
        my $mp3info = get_mp3info( $filename );
        $duration = int( $mp3info->{SECS} );
    }

    if ( ! $created ) {
        my @fileinfo = stat( $filename );
        $created = "epoch $fileinfo[9]";
    }

    my %record = (
        key       => Digest::SHA->new->addfile( $filename, 'b')->hexdigest(),
        created   => UnixDate( $created // 'now', '%Y-%m-%d %H:%M:%S'),
        duration  => $duration // 0,
        source_id => $source->id,
    );

    my $dbic   = $self->result_source->schema;
    my $guard  = $dbic->txn_scope_guard;
    my $record = $dbic->resultset('Record')->find_or_create(
        \%record,
        { key => 'key' },
    );

    $guard->commit;
    return $record;

}

;
