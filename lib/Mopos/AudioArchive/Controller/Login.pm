package Mopos::AudioArchive::Controller::Login;
use Moose;
use namespace::autoclean;

use Net::LDAP;
use Net::LDAP::Bind;
use Encode;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant REDIRECT_DEFAULT => '/';
use constant REDIRECT_MAP     => [
#    { role => 'root', url => '/users/' },
];

=head1 NAME

Mopos::AudioArchive::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) :FormConfig('login.yaml') {}

sub index_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    my $login    = $form->param('login');
    my $password = $form->param('password');

    my @auth = ({
        login    => $login,
        password => $password,
        deleted  => undef,
    });

    if ($ENV{LDAP_SERVER}) {
        my $id = $self->_ldap_auth($c, {
            login    => $login,
            password => $password,
        });

        if ($id) {
            @auth = ({ id => $id }, 'nopassword');
        }
    }

    if ($c->authenticate(@auth)) {
        $c->model('DB::Log')->create(
            {
                event         => 'user_login',
                user_id       => $c->user->id,
                address       => $c->req->address,
            }
        );

        my $redirect = REDIRECT_DEFAULT;

        ROLES:
        foreach my $role ( @{ REDIRECT_MAP() } ) {
            if ($c->check_user_roles( $role->{role} )) {
                $redirect = $role->{url};
                last ROLES;
            }
        }

#        $redirect ||= '/calls/';

        $c->response->redirect($c->uri_for($redirect));

        return;
    }
    else {
        $form->get_field('password')
            ->get_constraint({ type => 'Callback' })
            ->force_errors(1);
        $form->process;
    }

}

sub logout
:Global
:Args(0)
{
    my ( $self, $c ) = @_;
    $c->model('DB::Log')->create(
        {
            event         => 'user_logout',
            user_id       => $c->user->id,
            address       => $c->req->address,
        }
    );
    $c->delete_session('session expired');
    $c->logout;
    $c->response->redirect('/');
}

sub _ldap_auth {
    my ($self, $c, $args )  = @_;

    my $ldap = Net::LDAP->new($ENV{LDAP_SERVER}) // return; # TODO: Trace error
    my $dn   = sprintf($ENV{LDAP_USER_DN}, $args->{login});

    my $bind_result = $ldap->bind(
        $dn,
        password => $args->{password}
    );

    if ($bind_result->code) {
#        $c->journal('FAIL', {
#            reason   => 'LDAP: ' . $bind_result->error,
#            username => $args->{login},
#        });
        return; #TODO: trace error
    }

    my $search_result = $ldap->search(
        base   => $dn,
        scope  => 'base',
        filter => '(objectClass=*)',
    );

    if ($search_result->code) {
#        $c->app->log->debug($search_result->error);
        return;
    }

    my $ldap_user = $search_result->entry(0) // return;

    my $user = $c->model('DB::User')->update_or_create(
        {
            login      => $args->{login},
#            password   => $args->{password}, #TODO: nejaky random
            email      => $ldap_user->get_value('mail'),
            firstname  => decode_utf8($ldap_user->get_value('cn')),
            lastname   => decode_utf8($ldap_user->get_value('sn')),
        },
        { key => 'login' }
    );

#        $c->journal('LDAP user', { ldap_dn => $dn });

    $ldap->unbind;

    return $user->id;
}

=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
