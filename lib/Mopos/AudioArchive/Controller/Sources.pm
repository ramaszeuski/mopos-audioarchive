package Mopos::AudioArchive::Controller::Sources;
use Moose;
use namespace::autoclean;

use CGI;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

Mopos::AudioArchive::Controller::Sources - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('sources') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub json :Local {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model('DB::Source');

    my ( $columns, $sorting, $conditions )
        = $c->model('DB')->datatables_args( $args );

    $conditions->{deleted} = undef;
    $conditions->{hidden}  = 'f';

# TODO: autorizace, ale i pro system Broadcasts
#    if ( ! $c->stash->{is_admin} ) {
#        $conditions->{id} = $c->user->id;
#    }

    my $count = $model->count(
        $conditions,
    );

#    $args->{iDisplayLength} ||=100;
#    $args->{iDisplayStart}  ||=0;

    my $sources = $model->search(
        $conditions,
        {
            order_by => $sorting,
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
            columns  => [ @{$columns}, 'id', 'url' ]
        }
    );

    my @sources = ();

    SOURCE:
    while (my $source = $sources->next) {

        push @sources, {
            $source->get_columns,
            DT_RowId => $source->id,
        }
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@sources,
            iTotalRecords        => $count,
            iTotalDisplayRecords => $count,
        },
    );

}
=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
