package Mopos::AudioArchive::Controller::Root;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=head1 NAME

Mopos::AudioArchive::Controller::Root - Root Controller for Mopos::AudioArchive

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
}

sub auto :Private {
    my ($self, $c) = @_;

    if ( $c->user_exists ) {
        ROLE:
        foreach my $role ( $c->user->roles ) {
            $c->stash->{ 'is_' . $role } = 1;
        }
    }

    return 1;
}

=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;

# Jednoduchy dhandler
    my $template = $c->request->_path;
    $template  =~ s{/$}{\.html};

    my $file = join '/', (
        $c->config->{home} // '',
        $c->config->{'static'}{include_path}[0],
        $template,
    );

    if ( -f $file ) {
        $c->stash->{template} = $template;
    }
    else {
        $c->response->body( 'Page not found ' .  $file );
        $c->response->status(404);
    }
}

#sub favicon : Path('/favicon.ico') {
#    my ( $self, $c ) = @_;
#    $c->serve_static;
#}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : Private {
    my ($self, $c) = @_;

    if ( scalar @{ $c->error } ) {
        $c->stash->{errors}   = $c->error;
        for my $error ( @{ $c->error } ) {
            $c->log->error($error);
        }
        $c->clear_errors;
    }

    if ( $c->request->param('no_wrapper') ) {
        $c->stash->{no_wrapper} = 1;
    }

    if ( $c->res->status =~ /^[45]/ ) {
    ###TODO: error handler
    }
    elsif (
            ($c->req->param('output') && $c->req->param('output') eq 'json')
         || ($c->stash->{output} && $c->stash->{output} eq 'json')
    ) {
        $c->forward('View::JSON');
    }
    elsif ( (! $c->res->body) && ($c->res->status !~ /^3/) ) {
#        $c->stash->{navigation} = $c->navigation->get_navigation( $c );
        $c->forward('View::HTML');
    }
}

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
