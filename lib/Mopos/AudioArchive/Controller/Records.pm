package Mopos::AudioArchive::Controller::Records;
use Moose;
use namespace::autoclean;

use CGI;
use Mopos::AudioArchive::Storage;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

Mopos::AudioArchive::Controller::Records - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base :Chained('/') :PathPart('records') :CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub source :Chained('base') :PathPart('') :CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    my $source = $c->model('DB::Source')->search(
        {
            url => $id
        }
    )->first;

    die "Source '$id' not found!" if ! $source;

    $c->stash->{logo}   = $source->url;
    $c->stash->{title}  = $source->name;
    $c->stash->{source} = $source;

}

sub index :Chained('source') :PathPart('') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{mode} = 'paging';
}

sub archive :Chained('source') :PathPart('archive') :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash->{mode}     = 'paging';
    $c->stash->{template} = 'records/index.tt2';
}

sub json :Local {
    my ( $self, $c ) = @_;

    my $args  = $c->request->params;
    my $model = $c->model('DB::Record');


    my $source = $c->model('DB::Source')->search(
        {
            id => $args->{source_id},
        }
    )->first;

    return if ! $source;

    my $conditions = {
        deleted   => undef,
        source_id => $source->id,
    };

    my $count = $model->count(
        $conditions,
    );

    $args->{iDisplayLength} ||=10;
    $args->{iDisplayStart}  ||=0;

    my $records = $model->search(
        $conditions,
        {
            order_by => [ { -desc => 'created' } ],
            rows     => $args->{iDisplayLength},
            offset   => $args->{iDisplayStart},
        }
    );

    my $storage = Mopos::AudioArchive::Storage->new( $c->config->{workdir} );

    my @records = ();

    RECORD:
    while (my $record = $records->next) {

        my $basename = $storage->basename( $record->key );
        my $url      = $c->config->{records_base_url} . $basename;

        push @records, {
            $record->get_columns,
            url         => $url,
            url_escaped => CGI::escape( $url ),
            DT_RowId => $record->id,
        }
    }

    $c->stash(
        output => 'json',
        json    => {
            sEcho                => $args->{sEcho},
            aaData               => \@records,
            iTotalRecords        => $count,
            iTotalDisplayRecords => $count,
        },
    );

}

sub save_field :Local {
    my ( $self, $c ) = @_;

    $c->stash->{output} = 'json';
    my $args = $c->request->params;

    my $record = $self->_get($c, $args->{pk});

    if ( ! $record ) { # pokus editovat neexistujici nebo cizi zaznam
        $c->response->code(400);
        return;
    }

    $args->{value_orig} = $record->get_column($args->{name});

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $record->update( {
        $args->{name} => $args->{value},
    } );

    $c->model('DB::Log')->create(
        {
            event    => 'update_record',
            user_id  => $c->user->id,
            address  => $c->req->address,
            data     => $args,
        }
    );

    $scope_guard->commit;

    $c->stash->{json} = { rc => 'OK' };
}

sub delete :Local {
    my ( $self, $c ) = @_;

    my @ids = split /\D+/, $c->request->params->{id};
    my $records;

    if ( $c->stash->{is_admin} ) {
        $records = $c->model('DB::Record')->search({
            id => {-in => \@ids}
        });
    } else {
        $records = $c->model('DB::Record')->search({
            'me.id'     => { -in => \@ids },
            'source.id' => $c->user->id,
        },
        {
            join => 'source'
        });
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $records->update({ deleted => '\now()' });
    $c->model('DB::Log')->create(
        {
            event    => 'delete_records',
            user_id  => $c->user->id,
            address  => $c->req->address,
            data     => { id => \@ids },
        }
    );
    $scope_guard->commit;

    $c->response->status(204);
}

sub _get {
    my ( $self, $c, $id ) = @_;

    my $record = $c->model('DB::Record')->search({
        id      => $id,
        deleted => undef,
    })->first;

    if ( ! $c->stash->{is_admin} ) {
        $record = undef if $record->source->id != $c->user->id;
    }

    return $record;
}

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
