package Mopos::AudioArchive::Model::DB;
use Moose;
use namespace::autoclean;

use Date::Manip;

extends 'Catalyst::Model::DBIC::Schema';

our $VERSION = '0.01';
$VERSION = eval $VERSION;

__PACKAGE__->config(
    schema_class => 'Mopos::AudioArchive::Schema',
    'connect_info' => {
        on_connect_do  => [
            "set datestyle to German",
            "set timezone to 'Europe/Prague'",
        ],
        AutoCommit     => 1,
        quote_char     => q("),
        name_sep       => q(.),
        pg_enable_utf8 => 1,
    },
);

sub datatables_args {
    my $self = shift;
    my $args = shift;

    my $conditions = {};

    my @columns = ();
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;

        my $search = $args->{'sSearch_' . $n};

        if ( defined $search && length( $search) ) {
            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };
        }

        next COLUMN if ! exists $args->{sSearch};
        next COLUMN if ! length( $args->{sSearch} );


        if ( $args->{'bSearchable_' . $n} eq 'true' ) {
            $conditions->{$column} //= {
                ilike => '%' . $args->{sSearch} . '%',
            };
        }
    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
        push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"} ]};
    }

    return ( \@columns, \@sorting, $conditions );

}

sub make_conditions_from_filter {
    my $self   = shift;
    my $args   = shift;

    my $model  = $args->{model};
    my $filter = $args->{filter};
    my $fields = $args->{filter_fields};

    my @where = ();

    ITEM:
    foreach my $name ( keys %{ $filter } ) {

        my $item = $fields->{$name};

        $item->{type} //= 'eq';

        next ITEM if $item->{type} eq 'custom';

        my $value = $filter->{ $name } // next ITEM;

        if ( exists $item->{regexp} && $value !~ $item->{regexp}) {
            next ITEM;
        }

        if ( $item->{type} eq 'ilike' ) {
            $value = { 'ilike' => "%$value%" };
        }
        elsif ( $item->{type} eq 'like' ) {
            $value = { 'like' => '%' . $value . '%' };
        }
        elsif ( $item->{type} =~ /timestamp_(begin|end)/ ) {

            my $bound = $1;
            my $time;

            if ( exists $item->{time} ) {
                if ( $filter->{$item->{time}} =~ /\d{1,2}:\d{1,2}/) {
                    $time = $filter->{$item->{time}};
                }
            }

            if ( $bound eq 'begin' ) {
                $value = ParseDate( "$value " . ( $time // '00:00') );
                $value &&= { '>=' => UnixDate($value, '%Y-%m-%d %H:%M:00')};
            }
            else {
                $value = ParseDate( "$value " . ( $time // '23:59') );
                $value &&= { '<=' => UnixDate($value, '%Y-%m-%d %H:%M:59')};
            }
        }
        elsif ( $item->{type} eq 'range' ) {

            next ITEM if $value !~ /(\d+)\D+(\d+)/;
            next ITEM if $1 > $2;

            my ( $min, $max ) = ( $1, $2 );
            my $multiplier = $item->{multiplier} || 1;
            my @range = ();

            $min = undef if exists $item->{min} && $min <= $item->{min};
            $max = undef if exists $item->{max} && $max >= $item->{max};

            push @range, { '>=' => $min * $multiplier } if defined $min;
            push @range, { '<=' => $max * $multiplier } if defined $max;

            $value = \@range;

        }

        my @columns;

        if ( exists $item->{columns} && ref $item->{columns} eq 'ARRAY' ) {
            @columns = @{$item->{columns}};
        }
        elsif ( exists $item->{column}) {
            @columns = ( $item->{column} );
        }
        else {
            @columns = ( $name );
        }

        @columns = grep { $model->result_source->has_column($_) } @columns;
        my @values = ( ref $value eq 'ARRAY') ? @{ $value } : ( $value );

        if ( scalar @columns > 1 ) {
            foreach my $value ( @values ) {
                push @where, [
                    map { { $_ => $value } } @columns
                ];
            }
        }
        elsif ( scalar @columns ) {
            foreach my $value ( @values ) {
                push @where, { $columns[0] => $value };
            }
        }

    }

    return @where;
}

sub format_time {
    my $self = shift;
    my $sec  = shift;

    my $hour = int($sec / 3600);
    $sec    -= $hour * 3600;
    my $min  = int($sec / 60);
    $sec    -= $min * 60;

    my $hms = sprintf('%02d:%02d:%02d', $hour, $min, $sec);
    $hms =~ s/^00://;
    $hms =~ s/^0//;

    return $hms;
}

=head1 NAME

Mopos::AudioArchive::Model::DB - Catalyst DBIC Schema Model

=head1 DESCRIPTION

Catalyst Model.

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
