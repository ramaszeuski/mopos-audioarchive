package Mopos::AudioArchive::Storage;

use strict;
use warnings;
use File::Path qw(make_path);
use File::Copy;
use File::Slurp;
use File::MimeInfo;
use Digest::SHA;
use Date::Manip;
use XML::Simple;
use MP3::Info;
use Sys::Syslog;
use JSON;

use constant LAME         => '/usr/bin/lame';
use constant LAME_OPTIONS => [ '-S' ];

Date_Init("DateFormat=non-US");

our $VERSION = '0.02';

sub new {
    my $classname = shift;
    my $workdir   = shift || die "WorkDir directory not defined";
    my $self      = {};

    my @directories = (
        'storage',
        'inbox',
        'dropped',
    );

    DIRECTORY:
    foreach my $dirname ( @directories ) {
        my $path = "$workdir/$dirname";
        $self->{$dirname} = $path;
        eval {
            -r $path || make_path( $path ) || die "Can't create directory $path\n";
        };
    }

    bless ($self, $classname);
    return $self;
}

sub store {
    my $self = shift;
    my $file = shift;

    $self->trace("Filename $file", 'debug');

    my $xml = $file;
    $xml =~ s/mp3$/xml/;

    my $id  = Digest::SHA->new->addfile( $file, 'b')->hexdigest();
    $self->trace("ID $id", 'debug');

    my $fullname     = $self->fullname( $id, 'mp3' );
    my $xml_fullname = $self->fullname( $id, 'xml' );
    $self->trace("Fullname $fullname", 'debug');

    my %mime = ( mime => mimetype( $file ) );
    $self->trace("MIME " . to_json(\%mime), 'debug');

    my @fileinfo = stat( $file );
    my $created = "epoch $fileinfo[9]";
    $self->trace("Fileinfo " . (join ',',@fileinfo), 'debug' );

    eval {
        if ( $mime{mime} !~ /audio\/(mpeg|mp3)/) {
            if ( $mime{mime} =~ /audio/ ) {
                my $rc  = system(LAME, @{ LAME_OPTIONS() }, $file, $fullname);
                $self->drop($file,"Can't encode - $rc" );
                die "Can't encode - $rc" if $rc;
            }
            else {
                die "Unsupported type";
            }
        }
        else {
            move( $file, $fullname );
        }
    };

    if ($@) {
        $self->trace($@, 'error');
        return { error => $@ };
    }

    move( $xml, $xml_fullname ) if -f $xml;

    my $mp3info  = get_mp3info( $fullname );
    my $mp3tag   = get_mp3tag( $fullname );
    my $duration = int( $mp3info->{SECS} );
    $self->trace("MP3 Info " . to_json($mp3info));

    if ( $xml_fullname && -f $xml_fullname ) {

            my $xml = XMLin ( $xml_fullname );

            if ( $xml->{datum} && $xml->{cas} ) {
                $created = ParseDate($xml->{datum} . ' ' . $xml->{cas});
            }

            if ( $xml->{delka} ) {
                if ($xml->{delka} =~ /(\d\d):(\d\d):(\d\d)/ ) {
                    $duration = $1 * 3600 + $2 * 60 + $3;
                }
                elsif ($xml->{delka} =~ /(\d\d):(\d\d)/ ) {
                    $duration = $1 * 60 + $2;
                }
                elsif ($xml->{delka} =~ /(\d\d)/ ) {
                    $duration = $1;
                }
            }
    }

    my $result = {
        key       => $id,
        duration  => $duration // 0,
        created   => UnixDate( $created // 'now', '%Y-%m-%d %H:%M:%S'),
    };

    # datum vysilani z filename
    if ( $file =~ /ROR(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})\.mp3/) {
        $result->{created} = "$1-$2-$3 $4:$5:00";
    }

    # nazev nahravky z filename
    FIELD:
	foreach my $field (qw(TITLE ARTIST ALBUM COMMENT)) {
        my $part = $mp3tag->{$field} || last FIELD;
        $result->{name} .= $part;
	}

    $self->trace("Stored " . to_json($result));
    return $result
}

sub drop {
    my $self     = shift;
    my $file     = shift;
    my $error    = shift;

    my $xml = $file;
    $xml =~ s/mp3$/xml/;

    my $id       = Digest::SHA->new->addfile( $file, 'b')->hexdigest();
    my $dropname = $self->dropname( $id );

    move( $file, "$dropname.mp3" );
    move( $xml,  "$dropname.xml" ) if -f $xml;

    # ulozime metadata
    open my $META, ">$dropname.json";
    print $META to_json (
        {
            filename => $file,
            error    => $error,
        },
    );
    close $META;
}

sub basename {
    my $self = shift;
    my $id   = shift;
    my $ext  = shift // 'mp3';

    # nginx cache like
    $id =~ /(..)(.)$/;
    return "$2/$1/$id.$ext";

}


sub fullname {
    my $self = shift;
    my $id   = shift;
    my $ext  = shift // 'mp3';

    # nginx cache like
    $id =~ /(..)(.)$/;
    my $dir = $self->{storage} . "/$2/$1";

    if ( ! -d $dir ) {
        make_path($dir);
    }
    return "$dir/$id.$ext";
}

sub dropname {
    my $self = shift;
    my $id   = shift;
    my $day  = shift // 'today';

    my $dir = $self->{dropped} . UnixDate($day, '/%Y%m%d/');

    if ( ! -d $dir ) {
        make_path($dir);
    }
    return "$dir/$id";
}

sub trace {
    my $self = shift;
    my $message = shift;
    my $level   = shift || 'info';
    syslog($level, "STORAGE> $message");
    print "STORAGE[$level] $message\n" ;
}

__END__
