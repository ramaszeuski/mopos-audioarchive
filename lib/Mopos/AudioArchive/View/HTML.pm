package Mopos::AudioArchive::View::HTML;

use strict;
use warnings;

use base 'Catalyst::View::TT';

__PACKAGE__->config(
    render_die          => 1,
    ENCODING            => 'utf-8',
    TEMPLATE_EXTENSION  => '.tt2',
    WRAPPER             => 'wrapper.tt2',
    INCLUDE_PATH        => [
        'root/src',
        'root/lib',
        'root/site',
    ],
);

=head1 NAME

Mopos::AudioArchive::View::HTML - TT View for Mopos::AudioArchive

=head1 DESCRIPTION

TT View for Mopos::AudioArchive.

=head1 SEE ALSO

L<Mopos::AudioArchive>

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
