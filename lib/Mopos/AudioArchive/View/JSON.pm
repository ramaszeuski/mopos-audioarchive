package Mopos::AudioArchive::View::JSON;

use strict;
use base 'Catalyst::View::JSON';

__PACKAGE__->config(
    expose_stash => 'json',
);

=head1 NAME

Mopos::AudioArchive::View::JSON - Catalyst JSON View

=head1 SYNOPSIS

See L<Mopos::AudioArchive>

=head1 DESCRIPTION

Catalyst JSON View.

=head1 AUTHOR

Andrej Ramašeuski,,,,

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
