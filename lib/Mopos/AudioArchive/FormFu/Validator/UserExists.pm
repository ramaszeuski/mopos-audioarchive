package Mopos::AudioArchive::FormFu::Validator::UserExists;

use strict;
use utf8;

use base 'HTML::FormFu::Validator';

sub validate_value {
    my $self = shift;
    my ( $value, $params) = @_;
    my $c = $self->form->stash->{context};

    return 1 if $c->model('DB::User')->search( { login => $value } )->count;

    die HTML::FormFu::Exception::Validator->new({
        message => 'Uživatel se zadaným jménem neexistuje',
    });
}

1;

