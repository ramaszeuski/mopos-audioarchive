use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'Mopos::AudioArchive' }
BEGIN { use_ok 'Mopos::AudioArchive::Controller::Records' }

ok( request('/records')->is_success, 'Request should succeed' );
done_testing();
