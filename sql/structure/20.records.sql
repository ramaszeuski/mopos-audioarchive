create table "record" (
    "id"          integer not null default nextval('uid_seq'),
    "source_id"   integer not null,
    "key"         varchar(40) not null,
    "created"     timestamp(0),
    "processed"   timestamp(0) not null default now(),
    "deleted"     timestamp(0),

    "duration"    numeric not null,
    "name"        text,
    "description" text,
    primary key ("id"),
    foreign key ("source_id") references "source" ("id") on update cascade on delete cascade,
    unique("key")
);
