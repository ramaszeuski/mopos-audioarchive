create table "source" (
    "key" text,
    "url" text,
    "named_relations" boolean not null default false,
    "hidden" boolean not null default false,
    "last_record_created" timestamp(0)
    primary key ("id"),
    unique("key"),
    unique("url")
) inherits ("subject");

