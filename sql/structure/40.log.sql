create table "log" (
    "id"            integer not null default nextval('uid_seq'),
    "user_id"       integer,
    "timestamp"     timestamp(0) not null default current_timestamp,
    "event"         varchar(32) not null,
    "address"       varchar(15),
    "data"          text,
    primary key ("id")
);

create view "log_view" as
select
    "log".*,
    "users"."login" as "user_login",
    "users"."firstname" as "user_firstname",
    "users"."lastname" as "user_lastname"
    from "log"
    left join "users" on ("log"."user_id" = "users"."id")
;
