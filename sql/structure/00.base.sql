create sequence "uid_seq" start 100000;

create table "subject" (
    "id"        integer not null default nextval('uid_seq'),
    "active"    bool not null default true,
    "created"   timestamp(0) not null default now(),
    "deleted"   timestamp(0),
    "login"     varchar(32),
    "password"  varchar(32),
--jmeno, prijmeni / nazev
    "name"      text,
    "firstname" text,
    "lastname"  text,
--kontakty
    "email"     text,
    "messenger" text,
    "phone"     text,
    "cellular"  text,
    "fax"       text,
--adresa
    "country"   text,
    "region"    text,
    "city"      text,
    "address"   text,
    "zip"       text,
    "profile"   text,
    "notes"     text
);

