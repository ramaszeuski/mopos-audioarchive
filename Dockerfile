FROM registry.gitlab.com/ramaszeuski/catalyst-base:0.3.1

RUN useradd --no-log-init -u 1000 -r -g users audioarchive

RUN apt-get update && apt-get install -y \
 lame \
 locales \
 libmp3-info-perl \
 libnet-ldap-perl \
 libproc-processtable-perl \
 libfile-mimeinfo-perl

RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen

ADD . /opt/app

ENV CATALYST_CONFIG=/opt/app/mopos_audioarchive.yaml PERL5LIB=/opt/app/lib CATALYST_HOME=/opt/app
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

USER audioarchive
CMD /opt/app/script/mopos_audioarchive_server.pl
